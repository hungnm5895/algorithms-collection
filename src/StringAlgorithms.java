import java.util.ArrayList;

public class StringAlgorithms {

    public void printTest() {
        System.out.println("Hello, World!");
    }

    /**
     * Tách câu thành 1 mảng các từ, truyền vào 1 câu sẽ tách thành Arraylist ngăn cách bởi regex là khoảng trắn
     * 
     * @param : Câu đầu vào vào
     * @return : Array list gồm các từ
     */
    public ArrayList<String> splitStrings(String requestString) {
        ArrayList<String> splitStringArr = new ArrayList<>();
        String[] stringArr = requestString.split(" ");
        for (String stringElement : stringArr) {
            splitStringArr.add(stringElement);
        }
        return splitStringArr;
    }

    




}
