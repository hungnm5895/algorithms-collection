import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ArrayAlgorithms {
    

    /**
     * Thuật toán đảo ngược các phân tử của Arraylist
     * Sử dụng phương thức mặc định reverse() của lớp Collections
     * 
     * @param : Arraylist đầu vào
     * @return : Arraylist được đảo ngược phần tử gồm các từ
     */
    public ArrayList<Integer> reverseArrayList(ArrayList<Integer> lstInput) {
        Collections.reverse(lstInput);
        return lstInput;
    }


}
