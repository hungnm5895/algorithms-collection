import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class NumberALgorithms {

    /**
     * Hàm tạo số tự nhiên từ 1 - 10
     * Math.random() sẽ trả giá trị ngẫu nhiên từ 0 - 1 => nhân với số n sẽ ra số
     * ngẫu nhiên từ 0 - n
     * 
     * @Param: void
     * @Return: số tự nhiên ngẫu nhiên 1-10
     * 
     */
    public int getRandomInt() {
        double ketqua = Math.random() * 10 + 1;
        return (int) ketqua;
    }

    /**
     * Thuật toán sắp xếp tăng dần
     * Sử dụng phương thức mặc định sort() của lớp Collections
     * 
     * @param : Câu đầu vào vào
     * @return : Array list gồm các từ
     */
    public ArrayList<Integer> sortIntAsc(ArrayList<Integer> lstInput) {
        Collections.sort(lstInput);
        return lstInput;
    }

    /**
     * Thuật toán sắp xếp giảm dần
     * Sử dụng phương thức mặc định sort() của lớp Collections
     * 
     * @param : Câu đầu vào vào
     * @return : Array list gồm các từ
     */
    public ArrayList<Integer> sortIntDes(ArrayList<Integer> lstInput) {
        Collections.sort(lstInput, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 > o2 ? -1 : 1;
            }
        });
        return lstInput;
    }

    



}
