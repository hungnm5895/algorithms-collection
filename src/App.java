import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {

        StringAlgorithms tesAlgorithms = new StringAlgorithms();
        // tesAlgorithms.printTest();

        // Test sinh số ngẫu nhiên
        /*
         * NumberALgorithms newNumberALgorithms = new NumberALgorithms();
         * System.out.println("Tạo ngẫu nhiên số tự nhiên " +
         * newNumberALgorithms.getRandomInt());
         * System.out.println("Tạo ngẫu nhiên số tự nhiên " +
         * newNumberALgorithms.getRandomInt());
         * System.out.println("Tạo ngẫu nhiên số tự nhiên " +
         * newNumberALgorithms.getRandomInt());
         * System.out.println("Tạo ngẫu nhiên số tự nhiên " +
         * newNumberALgorithms.getRandomInt());
         * System.out.println("Tạo ngẫu nhiên số tự nhiên " +
         * newNumberALgorithms.getRandomInt());
         */

        // Test tách câu thanh các từ
        /*
         * StringAlgorithms newStringAlgorithms = new StringAlgorithms();
         * String inputString = "Câu này cần tách";
         * System.out.println(newStringAlgorithms.splitStrings(inputString));
         */

        NumberALgorithms newNumberALgorithms = new NumberALgorithms();
        ArrayList<Integer> lstInput = new ArrayList<>();
        lstInput.add(50);
        lstInput.add(1);
        lstInput.add(20);
        lstInput.add(100);
        lstInput.add(70);
        // Test thuật toán sắp xếp tăng/giảm dần
        // System.out.println(newNumberALgorithms.sortIntAsc(lstInput));
        // System.out.println(newNumberALgorithms.sortIntDes(lstInput));

        //Test thuật toán đảo ngược ArrayList 
        //ArrayAlgorithms newArrayAlgorithms = new ArrayAlgorithms();
        //System.out.println(lstInput);
        //System.out.println(newArrayAlgorithms.reverseArrayList(lstInput));

        
        
    }
}
